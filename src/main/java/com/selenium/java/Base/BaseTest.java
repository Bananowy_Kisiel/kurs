package com.selenium.java.base;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    private static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();

    public boolean instalApp = true;

    private static AppiumDriverLocalService server;



    public static void startAppiumServer() {
        AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder();
        serviceBuilder.withArgument(GeneralServerFlag.LOG_LEVEL,  "warn");
        serviceBuilder.withIPAddress("");
        serviceBuilder.usingAnyFreePort();

        server = AppiumDriverLocalService.buildService(serviceBuilder);
        server.start();
    }
    private static String platformName;
    private static ThreadLocal<String> webXmlTestName = new ThreadLocal<>();
    private static ThreadLocal<String> className = new ThreadLocal<>();
    protected void setupAndroidRemoteDriver(String platform, String deviceId, String deviceName, String xmlTestName) throws MalformedURLException{




        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("className", this.getClass().getSimpleName());
        capabilities.setCapability("deviceName", deviceName);
        capabilities.setCapability("currentXmlTest", xmlTestName);
        capabilities.setCapability(MobileCapabilityType.UDID, deviceId);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, platform);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);

        if(instalApp){

            String pathToFile = "" ;
            capabilities.setCapability(MobileCapabilityType.APP, pathToFile);
        }else{
            capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "pl.tablica");
        }

        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "pl.tablica2.app.startup.activity.StartupActivity");
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
        capabilities.setCapability("uiautomator2SerwerInstallTimeout", 90000);
        capabilities.setCapability("uiautomatorr2ServerLaunchTimeout", 90000);
        capabilities.setCapability(AndroidMobileCapabilityType.ANDROID_INSTALL_TIMEOUT, 10000);
        capabilities.setCapability(AndroidMobileCapabilityType.DEVICE_READY_TIMEOUT, 10000);
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60000);

        driver.set(new AndroidDriver<AndroidElement>(new URL(server.getUrl().toString()), capabilities));
    }


    public void launchAndroid(String platform, String deviceId, String deviceName, ITestContext context) {

        platformName = platform;

        try {
            setupAndroidRemoteDriver(platform, deviceId, deviceName, context.getCurrentXmlTest().getName());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        System.out.println("Session ID: " + driver.get().getSessionId());
        System.out.println("Server address: 127.0.0.1");
        System.out.println("Appium Port: " + server.getUrl().getPort());


        getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    protected static WebDriver getDriver() {
        return driver.get();
    }

    public boolean headless = false;
    public String url;

    private static ChromeDriverService service;

    public static void startChromeServer() {
        service = new ChromeDriverService.Builder()
                .usingAnyFreePort()
                .build();
        try {
            service.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void launchWeb(String platform, String browser, ITestContext context) {
        platformName = platform;
        if (browser.equals("Chrome")) {
            setupChromeWebRemoteDriver(platform, headless, context.getCurrentXmlTest().getName());
        } else if (browser.equals("Firefox")) {
            setupFirefoxWebRemoteDriver(platform, headless, context.getCurrentXmlTest().getName());
        } else {
            System.out.println("Incorrect Browser");
        }
        getDriver().manage().window().maximize();
        getDriver().manage().timeouts().pageLoadTimeout(55, TimeUnit.SECONDS);
        getDriver().manage().timeouts().implicitlyWait(55, TimeUnit.SECONDS);
        getDriver().navigate().to(url);
    }

    protected void setupChromeWebRemoteDriver(String platform, Boolean headless, String xmlTestName) {
        startChromeServer();
        platformName = platform;
        webXmlTestName.set(xmlTestName);
        className.set(this.getClass().getSimpleName());
        ChromeOptions chromeOptions = new ChromeOptions();
        if (headless) {
            chromeOptions.addArguments("--headless");
        }
        chromeOptions.addArguments("--window-size=2024,1024");
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("intl.accept_languages", "EN");
        chromeOptions.setExperimentalOption("prefs", prefs);
//        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        chromeOptions.addArguments("disable-extensions");
        chromeOptions.addArguments("ignore-certificate-errors");
        chromeOptions.addArguments("no-default-browser-check");
        chromeOptions.addArguments("test-type");
        driver.set(new RemoteWebDriver(service.getUrl(), chromeOptions));
    }

    protected void setupFirefoxWebRemoteDriver(String platform, Boolean headless, String xmlTestName) {
        platformName = platform;
        webXmlTestName.set(xmlTestName);
        className.set(this.getClass().getSimpleName());
        FirefoxBinary firefoxBinary = new FirefoxBinary();
        if (headless) {
            firefoxBinary.addCommandLineOptions("--headless");
        }
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("intl.accept_languages","en");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setProfile(firefoxProfile);
        firefoxOptions.setBinary(firefoxBinary);
        firefoxOptions.addArguments("--width=2024");
        firefoxOptions.addArguments("--height=1024");
        firefoxOptions.addPreference("geo.enabled", false);
        firefoxOptions.addPreference("geo.provider.use_corelocation", false);
        firefoxOptions.addPreference("geo.prompt.testing", false);
        firefoxOptions.addPreference("geo.prompt.testing.allow", false);
        firefoxOptions.addPreference("--headless", true);
//        firefoxOptions.addPreference("--lang", "PL");
        driver.set(new FirefoxDriver(firefoxOptions));
    }

    private static ThreadLocal<WebDriverWait> wait = new ThreadLocal<>();
    protected static WebDriverWait getWaitDriver (int time) {
        WebDriver currentDriver = getDriver();
        wait.set(new WebDriverWait(currentDriver, time));
        return wait.get();
    }

    }
