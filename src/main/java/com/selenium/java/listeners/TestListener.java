package com.selenium.java.listeners;

import com.selenium.java.base.BaseTest;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;

public class TestListener extends BaseTest implements ITestListener {

    @Attachment(value = "Page screenshot")
    public byte[] saveScreenshotPNG (WebDriver driver){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }
    @Attachment(value = "(0)", type = "text/plain")
    public static String saveTextLog (String message){
        return message;
    }
}