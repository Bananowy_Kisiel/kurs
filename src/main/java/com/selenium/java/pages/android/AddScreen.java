package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class AddScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/list_it")
    public WebElement btn_add;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement input_title;

    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_category;

    @FindBy(how = How.XPATH,  using = "//*[contains(@text, 'Sprzątanie')]")
    public WebElement btn_choose;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodatkowa / sezonowa')]")
    public WebElement btn_measurement;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Umowa zlecenie')]")
    public WebElement btn_contract;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Brak')]")
    public WebElement btn_special;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Od')]")
    public List<WebElement> input_od;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Do')]")
    public List<WebElement> input_do;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Opisz swoje ogłoszenie, używając przynajmniej 20 znaków. " +
            "Wpisz te informacje, które byłyby ważne dla Ciebie podczas przeglądania takiego ogłoszenia :)')]")
    public WebElement input_description;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Osoba kontaktowa*')]")
    public List<WebElement> input_person;

    @FindBy(how = How.ID, using = "pl.tablica:id/previewAdBtn")
    public  WebElement btn_see;

    public void addToApp(String title){
        Helper helper = new Helper();
        btn_add.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        input_title.sendKeys(title);
        btn_category.click();
        btn_choose.click();
        btn_measurement.click();
        btn_contract.click();
        btn_special.click();
        System.out.println("123");
        while (helper.swipeToElementByText("Opis*")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4); //od 0.2 do 0.8
        }
        System.out.println("1");
        for (int i = 0; i < input_od.size(); i++) {
            if (i == 1) {
                input_od.get(1).sendKeys("14");
            }
        }
        System.out.println("2");
        for (int j = 0; j < input_do.size(); j++) {
            if (j == 1) {
                input_do.get(2).sendKeys("18");
            }
        }
        input_description.sendKeys("Bla bla bla bla bla sprzataczka <3");
        while (helper.swipeToElementById("pl.tablica:id/previewAdBtn")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4); //od 0.2 do 0.8
        }
        System.out.println("3");
        for (int i = 0; i < input_person.size(); i++) {
            if (i == 1) {
                input_person.get(1).sendKeys("Don Lothario");
            }
        }
        btn_see.click();


    }
}
