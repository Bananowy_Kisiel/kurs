package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class FindingScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/searchInput")
    public WebElement input_search;

    @FindBy(how = How.ID, using = "pl.tablica:id/searchButton")
    public  WebElement btn_search;

    @FindBy(how = How.ID, using = "pl.tablica:id/title")
    public WebElement btn_destroyer;

    @FindBy(how = How.ID, using = "pl.tablica:id/action_favorite")
    public WebElement btn_heart;

    @FindBy(how = How.ID, using = "pl.tablica:id/linkUserAds")
    public WebElement btn_more;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Obserwuj')]")
    public  WebElement btn_follow;

    public void findingInApp (String title){
        Helper helper = new Helper();
        input_search.sendKeys(title);
        btn_search.click();
        btn_destroyer.click();
        while (helper.swipeToElementById("pl.tablica:id/recommended_ads_label")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4); //od 0.2 do 0.8
        }
        btn_heart.click();
        btn_more.click();
        btn_follow.click();


    }
}
