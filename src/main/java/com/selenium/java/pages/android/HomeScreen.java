package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomeScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/loginBtn")
    public WebElement btn_loginViaMail;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_login;

    @FindBy(how = How.ID, using = "pl.tablica:id/closeButton")
    public WebElement btn_close;



    public void loginToApp(String email, String password){

    btn_loginViaMail.click();
    input_email.sendKeys(email);
    input_password.sendKeys(password);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    btn_login.click();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void loginClose(){
        btn_close.click();
    }

}
