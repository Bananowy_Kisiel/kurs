package com.selenium.java.pages.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class RegistrationScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/createAccountBtn")
    public WebElement btn_registration;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement btn_approval;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_madeaccount;

    public void registrationToApp() {

        btn_registration.click();
        input_email.sendKeys("testerkurs01@gmail.com");
        input_password.sendKeys("FaceAccount1");

        Helper helper = new Helper();
        while (helper.swipeToElementById("pl.tablica:id/checkbox")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.8);
        }
        btn_approval.click();
        btn_madeaccount.click();
    }



    }
