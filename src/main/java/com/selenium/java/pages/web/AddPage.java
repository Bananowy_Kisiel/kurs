package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class AddPage extends BaseTest {

    //działa całość, świetnie Ci poszło Dominika <3

    @FindBy(how = How.XPATH, using = "//*[@id=\"listContainer\"]/div/div/div/a/span")
    public WebElement btn_add;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-title\"]")
    public  WebElement input_title;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetrenderSelect1-0\"]/dt/a")
    public  WebElement btn_category;

    @FindBy(xpath = "//strong[@class='category-name block lheight14 fnormal small' and contains(text(), 'Ubrania » Pozostałe')]")
    public WebElement btn_clothes;

    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/div/div[1]/input")
    public  WebElement input_price;

    @FindBy(how = How.XPATH, using = "//*[@id=\"parameter-div-price\"]/div[3]/div/div[1]/div/div[2]/label[2]")
    public  WebElement btn_nego;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetparam13\"]/li[2]/a/span[1]")
    public WebElement btn_used;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetparam123\"]/li[2]/a/span[1]")
    public WebElement btn_man;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetparam173\"]/dt/a")
    public  WebElement btn_size;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetparam173\"]/dd/ul/li[2]/a")
    public WebElement btn_xs;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetid_private_business\"]/li[2]/a/span[1]")
    public WebElement btn_person;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-description\"]")
    public WebElement input_description;

    @FindBy(how = How.ID, using = "add-file-1")
    public WebElement btn_upload;

    @FindBy(how = How.ID, using = "mapAddress")
    public  WebElement input_localisation;

    @FindBy(how = How.XPATH, using = "//*[@id=\"autosuggest-geo-ul\"]/li[1]")
    public WebElement btn_confirm;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-person\"]")
    public  WebElement input_name;

    @FindBy(how = How.ID, using = "add-phone")
    public  WebElement input_phone;

    @FindBy(how = How.ID, using = "preview-link")
    public  WebElement btn_see;


    public void addToApp(String title, String price, String description, String localisation, String name, String phone) throws AWTException {

        btn_add.click();
        input_title.sendKeys(title);
        btn_category.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_clothes.click();
        input_price.sendKeys(price);
        btn_nego.click();
        btn_used.click();
        btn_man.click();
        btn_size.click();
        btn_xs.click();
        Helper helper = new Helper();
        btn_person.click();
        input_description.sendKeys(description);
        btn_upload.click();
        helper.uploadImage("C:\\Users\\Webdeveloper102\\Desktop\\olx.jpg");
        input_localisation.sendKeys(localisation);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_confirm.click();
        input_phone.sendKeys(phone);
        input_name.sendKeys(name);
        btn_see.click();

    }
}
