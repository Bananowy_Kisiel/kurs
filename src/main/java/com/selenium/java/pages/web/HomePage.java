package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePage extends BaseTest {

    @FindBy(how = How.CLASS_NAME, using = "cookiesBarClose")
    public WebElement btn_cookies;

    @FindBy(how = How.ID, using = "my-account-link")
    public WebElement input_myAccout;

    @FindBy(how = How.ID, using = "userEmail")
    public  WebElement input_email;

    @FindBy(how = How.ID, using = "userPass")
    public  WebElement input_password;

    @FindBy(how = How.ID, using = "se_userLogin")
    public WebElement btn_login;


    public void loginToApp(String email, String password){

        btn_cookies.click();
        input_myAccout.click();
        input_email.sendKeys(email);
        input_password.sendKeys(password);
        btn_login.click();
        WebDriver driver = getDriver();
        Actions action = new Actions(driver);
        WebElement we = driver.findElement(By.id("my-account-link"));
        action.moveToElement(we).build().perform();
    }

}
