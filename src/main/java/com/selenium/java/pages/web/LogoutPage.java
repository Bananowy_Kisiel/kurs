package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LogoutPage extends BaseTest {

    @FindBy(how = How.ID, using = "login-box-logout")
     public WebElement btn_logout;

    public void logoutFromApp(){

        btn_logout.click();
    }
}
