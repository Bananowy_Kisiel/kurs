package com.selenium.java.pages.web;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchPage extends BaseTest {

    @FindBy(how = How.ID, using = "headerLogo")
    public WebElement btn_olx;

    @FindBy(how = How.ID, using = "headerSearch")
    public WebElement input_search;

    @FindBy(how = How.XPATH, using = "//*[@id=\"autosuggest-div\"]/ul/li[1]/a/span[1]")
    public WebElement btn_thiscat;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_subcat\"]/div[2]/a")
    public WebElement btn_category;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_subcat\"]/div[2]/ul/li[6]/a")
    public WebElement btn_headphones;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/a/span[2]")
    public WebElement btn_state;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_state\"]/div[2]/ul/li[3]/label[2]")
    public WebElement btn_new;

    @FindBy(how = How.XPATH, using = "//*[@id=\"tabs-container\"]/div/div[2]/ul/li[3]/a")
    public WebElement btn_company;

    @FindBy(how = How.XPATH, using = "//*[@id=\"viewSelector\"]/li[3]/a")
    public WebElement btn_blocks;

    public void searchInApp(String search) {

        btn_olx.click();
        input_search.sendKeys(search);
        btn_thiscat.click();
        btn_category.click();
        btn_headphones.click();
        System.out.println("po słuchawkach");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_state.click();
        System.out.println("rozwiniecie");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_new.click();
        System.out.println("nowy");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_company.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_blocks.click();
    }
}
