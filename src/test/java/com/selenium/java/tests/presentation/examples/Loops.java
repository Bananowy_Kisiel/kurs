package com.selenium.java.tests.presentation.examples;

public class Loops {
    public static void main(String[] args) {
        //whileLoop();
        //doWhileLoop();
        //forLoop();
        foreachLoop();
    }

    public static void whileLoop() {

    int licznik = 0;
    while (licznik < 10) {
        System.out.println("While Loop " + licznik);
        licznik++;
    }
    System.out.println("End of While Loop");
    }

    public static void doWhileLoop() {

        int licznik =0;

        do {
            System.out.println("This is do while Loop " + licznik);
            licznik++;
        }
        while (licznik <10);

        System.out.println("End of do while Loop");
    }

    public static void forLoop() {

        for(int i = 0; i < 10; i++) {
            System.out.println("This is FOR Loop " + i);
        }
        System.out.println("End of Loop");
    }

    public static void  foreachLoop() {

        int[] tablica = new int[10];

        for(int i = 0; i < 10; i++){
            tablica[i] = i + 1;
        }

        for(int x : tablica) {
            System.out.println(x);
        }
    }
}
