package com.selenium.java.tests.presentation.examples;

public class Person {
    void srednia(String name, double[] oceny) {

        double suma = 0;
        double srednia;

        for (int i = 0; i < oceny.length; i++) {
            suma = suma + oceny[i];
        }

        srednia = (Math.round((suma / oceny.length) * 100)) / 100.0;
        System.out.println("Dla ucznia " + name + " - średnia ocen wynosi " + srednia);
    }
}
