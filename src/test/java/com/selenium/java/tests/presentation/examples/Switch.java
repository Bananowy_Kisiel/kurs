package com.selenium.java.tests.presentation.examples;

public class Switch {

    private enum  Colors {
        BLUE,
        RED,
        YELLOW,
        PINK,
        NONE
    }
    public static void main(String[] args) {

        ChooseColor(Colors.PINK);
    }


    private static void ChooseColor(Colors colors) {

        switch (colors) {
            case RED:
                System.out.println("yey its red");
                break;

            case BLUE:
                System.out.println("yay its blue");
                break;

            case YELLOW:
                System.out.println("yay its yellow");
                break;

            case PINK:
                System.out.println("yay its pink");
                break;

            default:
                System.out.println("none...");
        }
    }


}
