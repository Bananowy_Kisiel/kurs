package com.selenium.java.tests.presentation.examples;
import org.testng.annotations.*;

public class TestStructure {
    @BeforeClass
    public void startup(){System.out.print("startup\n");}
    @BeforeMethod
    public void setup() {System.out.print("setup");}
    @Test public
    void test1() {System.out.print("test1");}
    @Test public
    void test2() {System.out.print("test2");}
    @Test public
    void test3() {System.out.print("test3");}
    @Test public
    void test4(){System.out.print("test4");}
    @AfterMethod
    public void teardown() {System.out.print("teardown\n");}
    @AfterClass
    public void stop() {System.out.print("stop");}
}
