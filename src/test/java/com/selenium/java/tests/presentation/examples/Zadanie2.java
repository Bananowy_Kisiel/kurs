package com.selenium.java.tests.presentation.examples;

import java.util.Arrays;

public class Zadanie2 {

            public enum Imiona {
                KASIA,
                BASIA,
                STASIA,
                MAJA,
                KAROLINA,
                ANIA,
                HANIA,
                LENA,
                WERONIKA,
                DOMINIKA;
            }

            public static String PrzypiszImie(Imiona imiona) {

                String imie;
                switch (imiona) {
                    case KASIA:
                        imie = "Kasia";
                        break;
                    case BASIA:
                        imie = "Basia";
                        break;
                    case STASIA:
                        imie = "Stasia";
                        break;
                    case MAJA:
                        imie = "Maja";
                        break;
                    case KAROLINA:
                        imie = "Karolina";
                        break;
                    case ANIA:
                        imie = "Ania";
                        break;
                    case HANIA:
                        imie = "Hania";
                        break;
                    case LENA:
                        imie = "Lena";
                        break;
                    case WERONIKA:
                        imie = "Weronika";
                        break;
                    case DOMINIKA:
                        imie = "Dominika";
                        break;
                    default:
                        imie = "brak";
                }
                return imie;
            }

            public static void main(String[] args) {
                String tablica[] = {
                        PrzypiszImie(Imiona.KASIA),
                        PrzypiszImie(Imiona.BASIA),
                        PrzypiszImie(Imiona.STASIA),
                        PrzypiszImie(Imiona.MAJA),
                        PrzypiszImie(Imiona.KAROLINA),
                        PrzypiszImie(Imiona.ANIA),
                        PrzypiszImie(Imiona.HANIA),
                        PrzypiszImie(Imiona.LENA),
                        PrzypiszImie(Imiona.WERONIKA),
                        PrzypiszImie(Imiona.DOMINIKA)};
                for (String imie : tablica) {
                    System.out.println(imie);
                }
                System.out.println();
                System.out.println("Rozmiar tablicy: " + tablica.length);
                System.out.println();
                System.out.println("Posortowane: ");

                Arrays.sort(tablica);
                for (String sort : tablica) {
                    System.out.println(sort);
                }
            }
        }



