package com.selenium.java.tests.test.android;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.android.AddScreen;
import com.selenium.java.pages.android.FindingScreen;
import com.selenium.java.pages.android.HomeScreen;
import com.selenium.java.pages.android.RegistrationScreen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class SimpleAndroidTest extends BaseTest {

    @BeforeClass
    public void startup(){

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setup(String platform, String deviceId, String deviceName, ITestContext context){

        instalApp = false;

        launchAndroid(platform, deviceId, deviceName, context);
    }

    @DataProvider
    public Object[][] getLogin() {
        return new Object[][]{
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"}
        };
    }

    @DataProvider
    public Object[][] getFailEmail() {
        return new Object[][]{
                {"1testerkurs01@gmail.com", "FaceAccount1"},
                {"2testerkurs01@gmail.com", "FaceAccount1"},
                {"3testerkurs01@gmail.com", "FaceAccount1"},
                {"4testerkurs01@gmail.com", "FaceAccount1"},
                {"5testerkurs01@gmail.com", "FaceAccount1"}
        };
    }

    @DataProvider
    public Object[][] getFailPass() {
        return new Object[][]{
                {"testerkurs01@gmail.com", "FaceAccou4nt1"},
                {"testerkurs01@gmail.com", "FaceAc5count1"},
                {"testerkurs01@gmail.com", "FaceAcco2unt1"},
                {"testerkurs01@gmail.com", "FaceAccou1nt1"},
                {"testerkurs01@gmail.com", "FaceAccou5nt1"}
        };
    }

    @Test(dataProvider = "getLogin")
    public void testLogin(String email, String pass){

        WebDriver driver = getDriver();

        HomeScreen homeScreen = PageFactory.initElements(driver, HomeScreen.class);
        homeScreen.loginToApp(email, pass);
        homeScreen.loginClose();
    }

    @Test(dataProvider = "getFailEmail")
    public void testFailLogin(String email, String pass){

        WebDriver driver = getDriver();

        HomeScreen homeScreen = PageFactory.initElements(driver, HomeScreen.class);
        homeScreen.loginToApp(email, pass);
    }

    @Test(dataProvider = "getFailPass")
    public void testFailLoginPass(String email, String pass){

        WebDriver driver = getDriver();

        HomeScreen homeScreen = PageFactory.initElements(driver, HomeScreen.class);
        homeScreen.loginToApp(email, pass);
    }

    @Test
    public void testRegistration() {

        WebDriver driver = getDriver();

        RegistrationScreen registrationScreen = PageFactory.initElements(driver, RegistrationScreen.class);
        registrationScreen.registrationToApp();

    }

    @Test
    public void testAdd() {

        WebDriver driver = getDriver();
        HomeScreen homeScreen = PageFactory.initElements(driver, HomeScreen.class);
        AddScreen addScreen = PageFactory.initElements(driver, AddScreen.class);
        homeScreen.loginToApp("testerkurs01@gmail.com", "FaceAccount1");
        addScreen.addToApp("Praca dla sprzątaczki");

    }

    @Test
    public  void testFinding() {

        WebDriver driver = getDriver();
        HomeScreen homeScreen = PageFactory.initElements(driver, HomeScreen.class);
        FindingScreen findingScreen = PageFactory.initElements(driver, FindingScreen.class);
        homeScreen.loginToApp("testerkurs01@gmail.com", "FaceAccount1");
        findingScreen.findingInApp("Lego Ninjago");

    }

    //@AfterMethod
    //public void tearDown(){
    //System.out.println("koniec...");
    //}
}
