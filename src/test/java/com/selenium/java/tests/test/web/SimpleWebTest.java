package com.selenium.java.tests.test.web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.listeners.TestListener;
import com.selenium.java.pages.web.AddPage;
import com.selenium.java.pages.web.HomePage;
import com.selenium.java.pages.web.LogoutPage;
import com.selenium.java.pages.web.SearchPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.awt.*;

@Listeners({TestListener.class})

public class SimpleWebTest extends BaseTest {

    Helper helper = new Helper();

    @BeforeMethod (description = "Starting appium server")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] simpleLogin() {
        return new Object[][]{
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"},
                {"testerkurs01@gmail.com", "FaceAccount1"}
        };
    }

    @DataProvider
    public Object[][] simpleFailEmail() {
        return new Object[][]{
                {"1testerkurs01@gmail.com", "FaceAccount1"},
                {"2testerkurs01@gmail.com", "FaceAccount1"},
                {"3testerkurs01@gmail.com", "FaceAccount1"},
                {"4testerkurs01@gmail.com", "FaceAccount1"},
                {"5testerkurs01@gmail.com", "FaceAccount1"}
        };
    }

    @DataProvider
    public Object[][] simpleFailPass() {
        return new Object[][]{
                {"testerkurs01@gmail.com", "FaceAccou4nt1"},
                {"testerkurs01@gmail.com", "FaceAc5count1"},
                {"testerkurs01@gmail.com", "FaceAcco2unt1"},
                {"testerkurs01@gmail.com", "FaceAccou1nt1"},
                {"testerkurs01@gmail.com", "FaceAccou5nt1"}
        };
    }

    @Test(dataProvider = "simpleLogin")
    public void simpletestLogin(String email, String pass){

        WebDriver driver = getDriver();

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.loginToApp(email, pass);

        Assert.assertFalse(homePage.btn_login.isDisplayed(), "Nie zalogowano");

        helper.getScreenShot("aa");
        helper.testScreenShoot("aa");

    }

    @Test(dataProvider = "simpleFailEmail")
    public void simpletestFailLogin(String email, String pass){

        WebDriver driver = getDriver();

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.loginToApp(email, pass);

        helper.getScreenShot("aa");
        helper.testScreenShoot("aa");

    }

    @Test(dataProvider = "simpleFailPass")
    public void simpletestFailLoginPass(String email, String pass){

        WebDriver driver = getDriver();

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.loginToApp(email, pass);

        helper.getScreenShot("aa");
        helper.testScreenShoot("aa");

    }

    @Test(description = "Checking basic functionality")
    public void simpletestAdd() throws AWTException {

        WebDriver driver = getDriver();

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AddPage addPage = PageFactory.initElements(driver, AddPage.class);
        homePage.loginToApp("testerkurs01@gmail.com", "FaceAccount1");
        addPage.addToApp("Komrza ministrancka", "200", "Komrza dla ministranta z takim czerwonym czymś :D",
                "86-031", "Dominika K.", "505025954");

        helper.getScreenShot("aa");
        helper.testScreenShoot("aa");

    }

    @Test
    public void simpletestSearch() {

        WebDriver driver = getDriver();

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        homePage.loginToApp("testerkurs01@gmail.com", "FaceAccount1");
        searchPage.searchInApp("jbl");

        helper.getScreenShot("aa");
        helper.testScreenShoot("aa");

    }

    @Test (description = "Checking basic functionality")
    public void simpletestLogout() {

        WebDriver driver = getDriver();

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        LogoutPage logoutPage = PageFactory.initElements(driver, LogoutPage.class);
        homePage.loginToApp("testerkurs01@gmail.com", "FaceAccount1");
        logoutPage.logoutFromApp();

        helper.getScreenShot("aa");
        helper.testScreenShoot("aa");

    }



    @AfterMethod
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        driver.close();
    }
}
